#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include<cmath>
using namespace std;


#define BOARD_SIZE  12      // This is the side length of the board
#define QUEENS 21           // This is the number of queens we are fitting on

class Board{
    int state[BOARD_SIZE][BOARD_SIZE]{};
    int oldState[BOARD_SIZE][BOARD_SIZE]{};
    int oldScore = QUEENS;
    string s = std::to_string(BOARD_SIZE) ;
    string sq = std::to_string(QUEENS) ;
    string runTitle = "run_" + s + "x" + s + "_board_" + sq + "_queens.csv" ;
    string boardTitle = "solved_" + s + "x" + s + "_board_" + sq + "_queens.csv" ;

public:
    float startTemp = 50;
    float maxTemp = 50;
    float minTemp = 0.00001;
    float tempStep = 1.1;
    float temp = startTemp;
    int printStep = 10000000;
    int score = QUEENS;
    bool solved = false;
    void printBoardToFile(string s);
    void initialiseBoard();
    void initialiseBoardFromFile(string );
    void printBoard();
    void swapRows();
    void swapColumns();
    void moveQueen();
    int countAttacks();
    int countTotalAttacks();
    int scoreBoard();
    void MCMove();
    void runSimulation();
};


// Run a MC simulation to find the optimum board
void Board::runSimulation() {
    temp = startTemp;
    ofstream myfile;            // Output logging file
    myfile.open (runTitle);
    myfile << "step,temp,score" << endl;
    int steps = 0;
    while (!solved){
        MCMove();
        steps ++;
        if (steps %printStep == 0) {
            temp /= tempStep;       // Decrease the temperature to simulate annealing
            myfile << steps << "," << temp << ","<< score << endl;
            cout << steps << "," << temp << ","<< score << endl;
//            printBoard();
            // Every so often, raise the temp back to the max temp to retry the annealing
            if (steps%1000000000 ==0 || temp < minTemp) temp = maxTemp;
        }
    }
    myfile.close();


    cout << "Board solved in " << steps << " steps" << endl;
    printBoard();
    printBoardToFile(boardTitle);

}


void Board::MCMove() {
    /*
     * Perform a move, then check if move is accepted. If not, return to old board state.
     */

    // Move the state to the old state
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            oldState[i][j] = state[i][j];
        }
    }
    
    // Pick randomly one of three possible moves. Swapping rows and columns is more rare
    int move = rand()%5;
    if (move ==0)           swapRows();
    else if (move == 1)     swapColumns();
    else moveQueen();

    score = scoreBoard();

    if (score > oldScore){ // Board has gotten worse. Accept with some probability, else return to the old board

        // METROPOLIS
        double dice = (float) rand() / RAND_MAX;
        double metropolis = pow(2.71, float(oldScore - score) / temp);
        if (metropolis< dice) { // Reset the board
            for (int i = 0; i < BOARD_SIZE; i++) {
                for (int j = 0; j < BOARD_SIZE; j++) {
                    state[i][j] = oldState[i][j];
                }
            }
            score = oldScore;
        }

//
//        // HARD DESCENT
//            for (int i = 0; i < BOARD_SIZE; i++) {
//                for (int j = 0; j < BOARD_SIZE; j++) {
//                    state[i][j] = oldState[i][j];
//                }
//            }
//            score = oldScore;

    }

    oldScore = score;

    if (score == 0) solved = true;

}
int Board::scoreBoard() {
    /*
     * Give the board an overall score. High is bad. Zero is solved.
     */
//    return countAttacks();
    return countTotalAttacks();

}
int Board::countAttacks(){
    /*
     * Estimate the number of attacks on the board. Superseded by countTotalAttacks() function.
     */
    int attacks = 0;
    bool blackQueen, whiteQueen,onBoard;
    int square_x, square_y;
    // Search along rows
    for (int i = 0; i < BOARD_SIZE; ++i)
    {
        blackQueen = false;
        whiteQueen = false;
        for (int j = 0; j < BOARD_SIZE; ++j)
        {
            if (state[i][j] == 1) blackQueen = true;
            if (state[i][j] == -1) whiteQueen = true;
        }
        if (whiteQueen && blackQueen) attacks++;
    }

    // Search along columns
    for (int i = 0; i < BOARD_SIZE; ++i)
    {
        blackQueen = false;
        whiteQueen = false;
        for (int j = 0; j < BOARD_SIZE; ++j)
        {
            if (state[j][i] == 1) blackQueen = true;
            if (state[j][i] == -1) whiteQueen = true;
        }
        if (whiteQueen && blackQueen) attacks++;
    }

    // Search along primary diagonal first half
    for (int i = 0; i < BOARD_SIZE; ++i){
        onBoard = true;
        blackQueen = false;
        whiteQueen = false;
        square_x = i;
        square_y = 0;
        while (onBoard){
            if (state[square_x][square_y] == 1) blackQueen = true;
            if (state[square_x][square_y] == -1) whiteQueen = true;
            square_x --;
            square_y ++;
            if (square_x > BOARD_SIZE-1 || square_x < 0|| square_y > BOARD_SIZE-1 || square_y < 0 ) onBoard = false;
        }
        if (whiteQueen && blackQueen) attacks++;

    }


    // Search along primary diagonal second half
    for (int i = 1; i < BOARD_SIZE; ++i){
        onBoard = true;
        blackQueen = false;
        whiteQueen = false;
        square_x = BOARD_SIZE-1;
        square_y = i;
        while (onBoard){
            if (state[square_x][square_y] == 1) blackQueen = true;
            if (state[square_x][square_y] == -1) whiteQueen = true;
            square_x --;
            square_y ++;
            if (square_x > BOARD_SIZE-1 || square_x < 0|| square_y > BOARD_SIZE-1 || square_y < 0 ) onBoard = false;
        }
        if (whiteQueen && blackQueen) attacks++;

    }

    // Search along other diagonal first half
    for (int i = 0; i < BOARD_SIZE; ++i){
        onBoard = true;
        blackQueen = false;
        whiteQueen = false;
        square_x = BOARD_SIZE -i -1;
        square_y = 0;
        while (onBoard){
            if (state[square_x][square_y] == 1) blackQueen = true;
            if (state[square_x][square_y] == -1) whiteQueen = true;
            square_x ++;
            square_y ++;
            if (square_x > BOARD_SIZE-1 || square_x < 0|| square_y > BOARD_SIZE-1 || square_y < 0 ) onBoard = false;
        }
        if (whiteQueen && blackQueen) attacks++;

    }

    // Search along other diagonal second half
    for (int i = 1; i < BOARD_SIZE; ++i){
        onBoard = true;
        blackQueen = false;
        whiteQueen = false;
        square_x = 0;
        square_y = i;
        while (onBoard){
            if (state[square_x][square_y] == 1) blackQueen = true;
            if (state[square_x][square_y] == -1) whiteQueen = true;
            square_x ++;
            square_y ++;
            if (square_x > BOARD_SIZE-1 || square_x < 0|| square_y > BOARD_SIZE-1 || square_y < 0 ) onBoard = false;
        }
        if (whiteQueen && blackQueen) attacks++;

    }


    return attacks;



}

int Board::countTotalAttacks(){
    /*
     * Count the total number of attacks on the board
     */

    int attacks = 0;
    bool onBoard;
    int whiteCount, blackCount;
    int square_x, square_y;
    
    // Search along rows
    for (int i = 0; i < BOARD_SIZE; ++i)
    {
        whiteCount = 0;
        blackCount = 0;
        for (int j = 0; j < BOARD_SIZE; ++j)
        {
            if (state[i][j] == 1) blackCount ++;
            if (state[i][j] == -1) whiteCount ++;
        }
        attacks += whiteCount * blackCount;
    }

    // Search along columns
    for (int i = 0; i < BOARD_SIZE; ++i)
    {
        whiteCount = 0;
        blackCount = 0;
        for (int j = 0; j < BOARD_SIZE; ++j)
        {
            if (state[j][i] == 1) blackCount ++;
            if (state[j][i] == -1) whiteCount ++;
        }
        attacks += whiteCount * blackCount;

    }



    // Search along primary diagonal first half
    for (int i = 0; i < BOARD_SIZE; ++i){
        onBoard = true;
        whiteCount = 0;
        blackCount = 0;
        square_x = i;
        square_y = 0;
        while (onBoard){
            if (state[square_x][square_y] == 1) blackCount ++;
            if (state[square_x][square_y] == -1) whiteCount ++;
            square_x --;
            square_y ++;
            if (square_x > BOARD_SIZE-1 || square_x < 0|| square_y > BOARD_SIZE-1 || square_y < 0 ) onBoard = false;
        }
        attacks += whiteCount * blackCount;

    }


    // Search along primary diagonal second half
    for (int i = 1; i < BOARD_SIZE; ++i){
        onBoard = true;
        whiteCount = 0;
        blackCount = 0;
        square_x = BOARD_SIZE-1;
        square_y = i;
        while (onBoard){
            if (state[square_x][square_y] == 1) blackCount ++;
            if (state[square_x][square_y] == -1) whiteCount ++;
            square_x --;
            square_y ++;
            if (square_x > BOARD_SIZE-1 || square_x < 0|| square_y > BOARD_SIZE-1 || square_y < 0 ) onBoard = false;
        }
        attacks += whiteCount * blackCount;

    }

    // Search along other diagonal first half
    for (int i = 0; i < BOARD_SIZE; ++i){
        onBoard = true;
        whiteCount = 0;
        blackCount = 0;
        square_x = BOARD_SIZE -i -1;
        square_y = 0;
        while (onBoard){
            if (state[square_x][square_y] == 1) blackCount ++;
            if (state[square_x][square_y] == -1) whiteCount ++;
            square_x ++;
            square_y ++;
            if (square_x > BOARD_SIZE-1 || square_x < 0|| square_y > BOARD_SIZE-1 || square_y < 0 ) onBoard = false;
        }
        attacks += whiteCount * blackCount;

    }

    // Search along other diagonal second half
    for (int i = 1; i < BOARD_SIZE; ++i){
        onBoard = true;
        whiteCount = 0;
        blackCount = 0;
        square_x = 0;
        square_y = i;
        while (onBoard){
            if (state[square_x][square_y] == 1) blackCount ++;
            if (state[square_x][square_y] == -1) whiteCount ++;
            square_x ++;
            square_y ++;
            if (square_x > BOARD_SIZE-1 || square_x < 0|| square_y > BOARD_SIZE-1 || square_y < 0 ) onBoard = false;
        }
        attacks += whiteCount * blackCount;

    }


    return attacks;



}


void Board::printBoard(){
    /*
     * Print the state of the board to the console
     */
    for (int i = 0; i < BOARD_SIZE; ++i){cout << "--";}
    cout << endl;

    for (int i = 0; i < BOARD_SIZE; ++i)
    {
        for (int j = 0; j < BOARD_SIZE; ++j)
        {
            cout << state[i][j] << ' ';
        }
        cout << endl;
    }

    for (int i = 0; i < BOARD_SIZE; ++i){cout << "--";}
    cout << endl;
}


void Board::printBoardToFile(string s){
    /*
     * Export the state of the board to a file, s
     */
    ofstream myfile;
    myfile.open(s);


    for (int i = 0; i < BOARD_SIZE; ++i)
    {
        for (int j = 0; j < BOARD_SIZE; ++j)
        {
            myfile << state[i][j] << ' ';
        }
        myfile << endl;
    }

}


void Board::initialiseBoard(){
    /*
     * Initialise board with random positions
     */
    // Set board to empty
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            state[i][j] = 0;
        }
    }

    // Place black queens randomly
    for (int i = 0; i < QUEENS; i++){
        int x = rand()%BOARD_SIZE;
        int y = rand()%BOARD_SIZE;
        if (state[x][y] == 0) state[x][y] = 1;
        else i = i -1; // Dont count that queen!
    }


    // Place white queens randomly
    for (int i = 0; i < QUEENS; i++){
        int x = rand()%BOARD_SIZE;
        int y = rand()%BOARD_SIZE;
        if (state[x][y] == 0) state[x][y] = -1;
        else i = i -1; // Dont count that queen!
    }


}


void Board::initialiseBoardFromFile(string s){
    /*
     * Read a file to use as the initial board setup.
     */

    ifstream inputFile;        // Input file stream object

    // Open the file.
    inputFile.open(s);
    int count = 0;             // Loop counter variable
    int count2 = 0;             // Loop counter variable

    // Read the numbers from the file into the array.

    while (count2 < BOARD_SIZE){
        while (count < BOARD_SIZE && inputFile >> state[count2][count]) {
            count++;
        }
        count2 ++;
        count = 0;
    }

    // Close the file.
    inputFile.close();




}


void Board::swapRows() {
    /*
     * Swap two randomly chosen rows
     */

    int row1 = rand()%BOARD_SIZE;
    int row2 = rand()%BOARD_SIZE;
    int tmp;

    for (int i = 0; i < BOARD_SIZE; i++){
        tmp = state[row2][i];
        state[row2][i] = state[row1][i];
        state[row1][i] = tmp;
    }
}

void Board::swapColumns() {
    /*
     * Swap two randomly chosen columns.
     */

    int row1 = rand()%BOARD_SIZE;
    int row2 = rand()%BOARD_SIZE;
    int tmp;

    for (auto & i : state){
        tmp = i[row2];
        i[row2] = i[row1];
        i[row1] = tmp;
    }
}


void Board::moveQueen(){
    /*
     * Move a single, randomly chosen queen into an adjacent, empty square.
     * If no empty squares found, the move does nothing.
     */

    // Pick queen
    bool queen = false;
    int x,y;
    while (!queen) {
        x = rand() % BOARD_SIZE;
        y = rand() % BOARD_SIZE;
        if (state[x][y] != 0) queen = true; // Have found a queen to move!
    }

    bool space_taken = true;
    int tries = 0;
    int vertical = 0;
    int horizontal = 0;
    int newx, newy;
    while (space_taken && tries < 10) {
        // pick direction
        vertical = (rand() % 2) - 1; // Between -1 and 1
        horizontal = (rand() % 2) - 1; // Between -1 and 1

        newx = x + horizontal;
        newy = y + vertical;
        if (newx > BOARD_SIZE-1) newx -= BOARD_SIZE;
        else if (newx < 0) newx += BOARD_SIZE;

        if (newy > BOARD_SIZE-1) newy -= BOARD_SIZE;
        else if (newy < 0) newy += BOARD_SIZE;


        if (state[newx][newy] == 0) space_taken = false; // Have found a space to move into
        tries ++;
    }

    if (tries < 9){
        state[newx][newy] = state[x][y];
        state[x][y] = 0;
    }



}


int main() {


    srand(2);

    // Set up board
    Board myBoard;
    myBoard.initialiseBoard();
    
    // Set up simulation parameters
    myBoard.maxTemp = (float)  QUEENS / (BOARD_SIZE) ;
    myBoard.startTemp = (float) 5* QUEENS / (BOARD_SIZE) ;
    myBoard.tempStep = 1.01;
    myBoard.minTemp = 0.00051;
    myBoard.printStep = 100000;
    
    // Run the simulation!
    myBoard.runSimulation();


    return 0;
}
